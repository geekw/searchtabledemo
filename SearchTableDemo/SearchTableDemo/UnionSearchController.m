//
//  UnionSearchController.m
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import "UnionSearchController.h"

@implementation UnionSearchController
#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}


#pragma mark - UISearchControllerDelegate

// Called after the search controller's search bar has agreed to begin editing or when
// 'active' is set to YES.
// If you choose not to present the controller yourself or do not implement this method,
// a default presentation is performed on your behalf.
//
// Implement this method if the default presentation is not adequate for your purposes.
//
- (void)presentSearchController:(UISearchController *)searchController {
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    //NSLog(@"willPresentSearchController");
    CGRect searchControllerFrame = CGRectMake(0, 0, CGRectGetWidth(searchController.view.frame), CGRectGetHeight(searchController.view.frame));
    searchController.view.frame = searchControllerFrame;
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    //NSLog(@"didPresentSearchController");
    CGRect searchControllerFrame = CGRectMake(0, 0, CGRectGetWidth(searchController.view.frame), CGRectGetHeight(searchController.view.frame));
    searchController.view.frame = searchControllerFrame;
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    //NSLog(@"willDismissSearchController");
}

- (void)didDismissSearchController:(UISearchController *)searchController {
    //NSLog(@"didDismissSearchController");
    if (self.unionSearchControllerDelegate && [self.unionSearchControllerDelegate respondsToSelector:@selector(didDismissSearchController:UnionSearchController:)]) {
        [self.unionSearchControllerDelegate didDismissSearchController:searchController UnionSearchController:self];
    }
    
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    if (self.unionSearchControllerDelegate && [self.unionSearchControllerDelegate respondsToSelector:@selector(updateSearchResultsForSearchController:UnionSearchController:)]) {
        [self.unionSearchControllerDelegate updateSearchResultsForSearchController:searchController UnionSearchController:self];
    }
}

@end
