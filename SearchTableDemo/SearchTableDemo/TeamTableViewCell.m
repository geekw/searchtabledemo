//
//  TeamTableViewCell.m
//  SearchTableDemo
//
//  Created by SINA on 14/11/13.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import "TeamTableViewCell.h"
static NSString *SelectedTeamListKey = @"SelectedTeamListKey";


@interface TeamTableViewCell()
@property (strong, nonatomic) IBOutlet UILabel *teamName;
@property (nonatomic,strong) IBOutlet UIButton *favoriteButton;

-(IBAction)favoriteButtonAction:(id)sender;
@end

@implementation TeamTableViewCell

-(id)init{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    return self;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(IBAction)favoriteButtonAction:(id)sender
{
    [self.favoriteButton setSelected:![self.favoriteButton isSelected]];
    if (self.teamTableViewCellDelegate && [self.teamTableViewCellDelegate respondsToSelector:@selector(favoriteButtonAction:SPMyTeamCell:)]) {
        [self.teamTableViewCellDelegate favoriteButtonAction:sender SPMyTeamCell:self];
    }
}

-(void)updateCell:(id)item
{
    if (item) {
        if ([item isKindOfClass:[NSString class]]) {
            self.teamName.text = item;
            
            NSMutableArray *selectedTeamList = [[NSUserDefaults standardUserDefaults] objectForKey:SelectedTeamListKey];
            self.favoriteButton.selected = [selectedTeamList containsObject:item];
        }

    }
}
@end
