//
//  ViewController.m
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import "ViewController.h"
#import "ResultTableController.h"
#import "TableViewAnimationUtil.h"
//#import "DeprecatedSearchController.h"

@interface ViewController ()<UISearchDisplayDelegate>//<UnionSearchControllerDelegate>

@property (nonatomic, strong) IBOutlet UITableView *searchTableView;
//完整的数据列表
@property (nonatomic, strong) MainTableViewController *mainTableViewController;
// our secondary search results table view
@property (nonatomic, strong) ResultTableController *resultTableController;
//search delegate
@property (nonatomic, strong) UISearchDisplayController *searchController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"球队管理";
    _mainTableViewController = [[MainTableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultTableController = [[ResultTableController alloc] init];
//    _mainTableViewController.tableView = self.searchTableView;

    

    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 44.0)];
    
    self.searchTableView.tableHeaderView = searchBar;
    self.searchTableView.delegate = self.mainTableViewController;
    self.searchTableView.dataSource = self.mainTableViewController;
    self.mainTableViewController.sourceListData = [NSMutableArray arrayWithObjects:@"chocolate Chip",@"dark chocolate",@"lollipop", @"candy cane",@"jaw breaker",@"caramel",@"sour chew",@"gummi bear",nil];

    
    _searchController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    self.searchController.delegate = self;
    self.searchController.searchResultsDataSource = self.resultTableController;
    self.searchController.searchResultsDelegate = self.resultTableController;
    
    
    [TableViewAnimationUtil addLongPressToTableView:self.searchTableView];
//    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognized:)];
//    [self.searchTableView addGestureRecognizer:longPress];
    

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    ResultTableController *tableController = (ResultTableController *)controller.searchResultsDataSource;
    tableController.filterListData = [self filterContentForSearchText:searchString];
//    [tableController.tableView reloadData];
    return YES;

}

-(NSMutableArray *)filterContentForSearchText:(NSString *)searchText
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"self contains[c] %@",searchText];
    NSMutableArray *searchListData = [NSMutableArray arrayWithArray:[self.mainTableViewController.sourceListData filteredArrayUsingPredicate:resultPredicate]] ;
    return searchListData;
}






@end
