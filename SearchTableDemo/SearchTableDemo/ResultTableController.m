//
//  ResultTableControllerTableViewController.m
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import "ResultTableController.h"
#import "TeamTableViewCell.h"
static NSString *TeamTableViewCellIdentifier = @"TeamTableViewCellIdentifier";
static NSString *SelectedTeamListKey = @"SelectedTeamListKey";

@interface ResultTableController ()<TeamTableViewCellDelegate>

@end

@implementation ResultTableController

- (void)viewDidLoad {
    [super viewDidLoad];

    UINib *teamCellNib = [UINib nibWithNibName:@"TeamTableViewCell" bundle:nil];
    [self.tableView registerNib:teamCellNib forCellReuseIdentifier:TeamTableViewCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filterListData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *content = [self.filterListData objectAtIndex:indexPath.row];
    TeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TeamTableViewCellIdentifier];
    if (!cell) {
        cell = [[TeamTableViewCell alloc] init];
    }
    [cell updateCell:content];
    cell.teamTableViewCellDelegate = self;
    return cell;
}


-(void)favoriteButtonAction:(id)sender SPMyTeamCell:(TeamTableViewCell*)myTeamCell;
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:myTeamCell];
    id currentItem = [self.filterListData objectAtIndex:indexPath.row];
    NSMutableArray *selectedTeamList = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SelectedTeamListKey]];
    if (!selectedTeamList) {
        selectedTeamList = [[NSMutableArray alloc] init];
    }
    UIButton *favoriteBtn = (UIButton *)sender;
    BOOL isFavorite = favoriteBtn.isSelected;
    if (isFavorite) {
        //收藏
        [selectedTeamList addObject:currentItem];
    }
    else
    {
        //取消
        [selectedTeamList removeObject:currentItem];
    }
    [[NSUserDefaults standardUserDefaults] setObject:selectedTeamList forKey:SelectedTeamListKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
