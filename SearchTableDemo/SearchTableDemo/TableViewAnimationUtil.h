//
//  TableViewAnimationUtil.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/18.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MainTableViewController.h"


@interface TableViewAnimationUtil : NSObject

+(void)addLongPressToTableView:(UITableView *)tableView;
@end
