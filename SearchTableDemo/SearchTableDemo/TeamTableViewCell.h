//
//  TeamTableViewCell.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/13.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TeamTableViewCell;
@protocol TeamTableViewCellDelegate <NSObject>
/*
 收藏按钮点击的委托
 */
-(void)favoriteButtonAction:(id)sender SPMyTeamCell:(TeamTableViewCell*)myTeamCell;

@end

@interface TeamTableViewCell : UITableViewCell
@property (nonatomic,assign) id<TeamTableViewCellDelegate> teamTableViewCellDelegate;

-(void)updateCell:(id)item;
@end
