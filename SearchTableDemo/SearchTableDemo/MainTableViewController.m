//
//  SearchTableViewController.m
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import "MainTableViewController.h"
#import "SectionHeadView.h"
#import "TeamTableViewCell.h"

static NSString *TeamTableViewCellIdentifier = @"TeamTableViewCellIdentifier";
static NSString *SelectedTeamListKey = @"SelectedTeamListKey";

@interface MainTableViewController ()<TeamTableViewCellDelegate>

@end

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *teamCellNib = [UINib nibWithNibName:@"TeamTableViewCell" bundle:nil];
    [self.tableView registerNib:teamCellNib forCellReuseIdentifier:TeamTableViewCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSMutableArray *)showListData
{
    _showListData = [NSMutableArray arrayWithArray:self.selectedListData];
    [_showListData addObjectsFromArray:self.sourceListData];
    return _showListData;
}

-(NSMutableArray *)selectedListData
{
    _selectedListData = [[NSUserDefaults standardUserDefaults] objectForKey:SelectedTeamListKey];
    if (!_selectedListData) {
        _selectedListData = [[NSMutableArray alloc] init];
    }
    return _selectedListData;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionHeadView *sectionHeadView = [[SectionHeadView alloc] init];
    if (section == 0) {
        sectionHeadView.mainTitle.text = @"我的球队";
        sectionHeadView.subtitle.text = @"长按拖动排序,足蓝分设主队";
    }
    else if (section == 1)
    {
        sectionHeadView.mainTitle.text = @"查找球队";
    }
    return sectionHeadView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    if (section == 0) {
        count = self.selectedListData.count;
    }
    else if(section == 1)
    {
        count = self.sourceListData.count;
    }
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *content = [self.showListData objectAtIndex:indexPath.row];
    if (indexPath.section == 0) {
        content = [self.selectedListData objectAtIndex:indexPath.row];
    }
    else if(indexPath.section == 1)
    {
        content = [self.sourceListData objectAtIndex:indexPath.row];
    }
    
    TeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TeamTableViewCellIdentifier];
    if (!cell) {
        cell = [[TeamTableViewCell alloc] init];
    }
    [cell updateCell:content];
    cell.teamTableViewCellDelegate = self;
    return cell;
}



-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [self.showListData exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
}

-(void)favoriteButtonAction:(id)sender SPMyTeamCell:(TeamTableViewCell*)myTeamCell;
{
    NSLog(@"%@",NSStringFromSelector(_cmd));
    NSIndexPath *indexPath = [self.tableView indexPathForCell:myTeamCell];
    if(!indexPath)
    {
        return;
    }
    if (indexPath.section == 0) {
        NSMutableArray *selectedTeamList = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:SelectedTeamListKey]];
        if (!selectedTeamList) {
            selectedTeamList = [[NSMutableArray alloc] init];
        }
        
        
        UIButton *favoriteBtn = (UIButton *)sender;
        BOOL isFavorite = favoriteBtn.isSelected;
        if (isFavorite) {
            //收藏
            //        selectedTeamList
        }
        else
        {

            NSArray *indexPathArray = [NSArray arrayWithObject:indexPath];
            [selectedTeamList removeObjectAtIndex:indexPath.row];
            [[NSUserDefaults standardUserDefaults] setObject:selectedTeamList forKey:SelectedTeamListKey];
            //取消
            [self.tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationAutomatic];
            
        }

    }
    

    
}
@end
