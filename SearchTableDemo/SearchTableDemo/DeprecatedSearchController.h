//
//  DeprecatedSearchController.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/17.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DeprecatedSearchController : NSObject
@property (nonatomic, strong) UISearchDisplayController *searchController;

@end
