//
//  SectionHeadView.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/11.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeadView : UIView
@property (strong, nonatomic) IBOutlet UILabel *mainTitle;
@property (strong, nonatomic) IBOutlet UILabel *subtitle;

@end
