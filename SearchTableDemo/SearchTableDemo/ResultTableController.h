//
//  ResultTableControllerTableViewController.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultTableController : UITableViewController
@property(nonatomic,strong) NSMutableArray *filterListData;
@end
