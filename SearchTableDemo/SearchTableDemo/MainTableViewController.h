//
//  SearchTableViewController.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewController : UITableViewController
/**
 *  原始数据
 */
@property(nonatomic,strong) NSMutableArray *sourceListData;
/**
 *  选中的数据
 */
@property(nonatomic,strong) NSMutableArray *selectedListData;
/**
 *  选中的数据+原始数据，展示
 */
@property(nonatomic,strong) NSMutableArray *showListData;
@end
