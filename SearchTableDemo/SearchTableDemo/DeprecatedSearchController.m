//
//  DeprecatedSearchController.m
//  SearchTableDemo
//
//  Created by SINA on 14/11/17.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import "DeprecatedSearchController.h"

@implementation DeprecatedSearchController

- (instancetype)init
{
    self = [super init];
    if (self) {
        _searchController = [[UISearchDisplayController alloc] init];
    }
    return self;
}
@end
