//
//  UnionSearchController.h
//  SearchTableDemo
//
//  Created by SINA on 14/11/10.
//  Copyright (c) 2014年 Sport. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class UnionSearchController;

@protocol UnionSearchControllerDelegate <NSObject>
- (void)updateSearchResultsForSearchController:(UISearchController *)searchController UnionSearchController:(UnionSearchController *)unionSearchController;

- (void)didDismissSearchController:(UISearchController *)searchController UnionSearchController:(UnionSearchController *)unionSearchController;
@end

@interface UnionSearchController : NSObject<NSObject,UISearchResultsUpdating,UISearchBarDelegate,UISearchControllerDelegate>
@property (nonatomic,assign) id<UnionSearchControllerDelegate> unionSearchControllerDelegate;

@end
